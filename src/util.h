#ifndef LAMBDA_UTIL_H
#define LAMBDA_UTIL_H


template <class T>
const T &operator||(const T &left, const T &right) {
    return left ? left : right;
}


#endif //LAMBDA_UTIL_H
