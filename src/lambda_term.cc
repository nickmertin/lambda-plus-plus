#include "lambda_term.h"
#include "util.h"

std::string lambda_term::to_tex(bool rightmost) const {
    return to_string(rightmost);
}

variable_term::variable_term(const std::string &name) : name(name) {}

std::shared_ptr<const lambda_term> variable_term::reduce() const {
    return nullptr;
}

std::shared_ptr<const lambda_term> variable_term::substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const {
    return variable == name ? value : nullptr;
}

std::string variable_term::to_string(bool rightmost) const {
    return name;
}

abstraction_term::abstraction_term(const std::string &input, const std::shared_ptr<const lambda_term> &expression) : input(input), expression(expression) {}

std::shared_ptr<const lambda_term> abstraction_term::reduce() const {
    return [&](const auto &x) {
        return x ? std::shared_ptr<const lambda_term>(new abstraction_term(input, x)) : nullptr;
    }(expression->reduce());
}

std::shared_ptr<const lambda_term> abstraction_term::substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const {
    return variable == input ? nullptr : std::shared_ptr<const lambda_term>(new abstraction_term(input, std::shared_ptr<const lambda_term>(new substitution_term(expression, variable, value))));
}

std::string abstraction_term::to_string(bool rightmost) const {
    std::string s = "\\" + input + "." + expression->to_string();
    return rightmost ? s : '(' + s + ')';
}

std::string abstraction_term::to_tex(bool rightmost) const {
    std::string s = "\\lambda " + input + "." + expression->to_tex();
    return rightmost ? s : '(' + s + ')';
}

application_term::application_term(const std::shared_ptr<const lambda_term> &left, const std::shared_ptr<const lambda_term> &right) : left(left), right(right) {}

std::shared_ptr<const lambda_term> application_term::reduce() const {
    return [&](const auto &x) {
        return x ? std::shared_ptr<const lambda_term>(new application_term(x, right)) : [&](const auto &y) {
            return y ? std::shared_ptr<const lambda_term>(new substitution_term(y->expression, y->input, right)) : [&](const auto &z) {
                return z ? std::shared_ptr<const lambda_term>(new application_term(left, z)) : nullptr;
            }(right->reduce());
        }(std::dynamic_pointer_cast<const abstraction_term>(left));
    }(left->reduce());
}

std::shared_ptr<const lambda_term> application_term::substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const {
    return std::shared_ptr<const lambda_term>(new application_term(left->substitute(variable, value) || left, right->substitute(variable, value) || right));
}

std::string application_term::to_string(bool rightmost) const {
    std::string &&_r = right->to_string(rightmost);
    if (dynamic_cast<const application_term *>(right.get()))
        _r = '(' + _r + ')';
    return left->to_string(false) + ' ' + _r;
}

std::string application_term::to_tex(bool rightmost) const {
    std::string &&_r = right->to_tex(rightmost);
    if (dynamic_cast<const application_term *>(right.get()))
        _r = '(' + _r + ')';
    return left->to_tex(false) + _r;
}

substitution_term::substitution_term(const std::shared_ptr<const lambda_term> &expression, const std::string &variable, const std::shared_ptr<const lambda_term> &value) : expression(expression), variable(variable), value(value) {}

std::shared_ptr<const lambda_term> substitution_term::reduce() const {
    return expression->substitute(variable, value) || expression;
}

std::shared_ptr<const lambda_term> substitution_term::substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const {
    return [&](const auto &x) {
        return x ? std::shared_ptr<const lambda_term>(new substitution_term(x, this->variable, this->value)) : nullptr;
    }(expression->substitute(variable, value));
}

std::string substitution_term::to_string(bool rightmost) const {
    std::string &&_l = expression->to_string(false);
    if (dynamic_cast<const application_term *>(expression.get()))
        _l = '(' + _l + ')';
    return _l + '[' + variable + ":=" + value->to_string() + ']';
}

std::string substitution_term::to_tex(bool rightmost) const {
    std::string &&_l = expression->to_tex(false);
    if (dynamic_cast<const application_term *>(expression.get()))
        _l = '(' + _l + ')';
    return _l + '[' + variable + ":=" + value->to_tex() + ']';
}
