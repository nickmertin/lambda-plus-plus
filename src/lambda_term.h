#ifndef LAMBDA_LAMBDA_TERM_H
#define LAMBDA_LAMBDA_TERM_H


#include <memory>

class lambda_term {
public:
    virtual std::shared_ptr<const lambda_term> reduce() const = 0;
    virtual std::shared_ptr<const lambda_term> substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const = 0;
    virtual std::string to_string(bool rightmost = true) const = 0;
    virtual std::string to_tex(bool rightmost = true) const;
};

class variable_term final : public lambda_term {
    const std::string name;

public:
    explicit variable_term(const std::string &name);

    std::shared_ptr<const lambda_term> reduce() const override;
    std::shared_ptr<const lambda_term> substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const override;
    std::string to_string(bool rightmost) const override;
};

class abstraction_term final : public lambda_term {
    const std::string input;
    const std::shared_ptr<const lambda_term> expression;

    friend class application_term;
    
public:
    abstraction_term(const std::string &input, const std::shared_ptr<const lambda_term> &expression);

    std::shared_ptr<const lambda_term> reduce() const override;
    std::shared_ptr<const lambda_term> substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const override;
    std::string to_string(bool rightmost) const override;
    std::string to_tex(bool rightmost) const override;
};

class application_term final : public lambda_term {
    const std::shared_ptr<const lambda_term> left;
    const std::shared_ptr<const lambda_term> right;

public:
    application_term(const std::shared_ptr<const lambda_term> &left, const std::shared_ptr<const lambda_term> &right);

    std::shared_ptr<const lambda_term> reduce() const override;
    std::shared_ptr<const lambda_term> substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const override;
    std::string to_string(bool rightmost) const override;
    std::string to_tex(bool rightmost) const override;
};

class substitution_term final : public lambda_term {
    const std::shared_ptr<const lambda_term> expression;
    const std::string variable;
    const std::shared_ptr<const lambda_term> value;
public:
    substitution_term(const std::shared_ptr<const lambda_term> &expression, const std::string &variable, const std::shared_ptr<const lambda_term> &value);

    std::shared_ptr<const lambda_term> reduce() const override;
    std::shared_ptr<const lambda_term> substitute(const std::string &variable, const std::shared_ptr<const lambda_term> &value) const override;
    std::string to_string(bool rightmost) const override;
    std::string to_tex(bool rightmost) const override;
};


#endif //LAMBDA_LAMBDA_TERM_H
