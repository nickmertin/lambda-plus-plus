#include <iostream>
#include "lambda_term.h"
#include "cli.h"
#include "parse.h"

int main(int argc, char **argv) {
    std::string input = read_cli(argc, argv);
    if (input.empty() && !cgi_flag) {
        std::cout << "lambda++ Lambda Calculus REPL\n";
        std::cout << "Type ';h' for help\n";
        while (true) {
            std::cout << '>';
            std::getline(std::cin, input);
            if (input.empty() || input == ";")
                continue;
            if (input[0] == ';') {
                switch (input[1]) {
                    case 'h':
                        std::cout << ";h\thelp\n";
                        std::cout << ";q\tquit\n";
                        break;
                    case 'q':
                        return 0;
                    default:
                        std::cout << "Unrecognized command '" << input[1] << "'\n";
                }
            } else {
                std::shared_ptr<const lambda_term> expr = parse(input), next{};
                for (int i = 0; (next = expr->reduce()) && i < max_iterations; ++i) {
                    if (verbose_flag)
                        std::cout << expr->to_string() << '\n';
                    expr = std::move(next);
                }
                std::cout << expr->to_string() << '\n';
                if (next) {
                    std::cout << "Maximum iterations reached (" << max_iterations << ")\n";
                }
            }
        }
    } else {
        std::shared_ptr<const lambda_term> expr = parse(input), next{};
        if (cgi_flag) {
            std::cout << "Content-type: text/plain\r\n\r\n";
        }
        for (int i = 0; (next = expr->reduce()) && i < max_iterations; ++i) {
            if (verbose_flag)
                std::cout << "$$" << expr->to_tex() << "$$\n";
            expr = std::move(next);
        }
        std::cout << "$$" << expr->to_tex() << "$$\n";
        if (next) {
            std::cout << "Maximum iterations reached (" << max_iterations << ")\n";
        }
        return 0;
    }
}
