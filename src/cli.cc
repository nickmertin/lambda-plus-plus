#include <iostream>
#include <sstream>
#include <limits>
#include <getopt.h>
#include "cli.h"
#include "util.h"

int verbose_flag = 0;
int cgi_flag = MODE_PURE;
int max_iterations = std::numeric_limits<int>::max();
cli_mode_t cli_mode;

std::string read_cli(int argc, char **argv) {
    const option opts[] = {
            {"verbose",     no_argument,        &verbose_flag,  1},
            {"brief",       no_argument,        &verbose_flag,  0},
            {"cgi",         no_argument,        &cgi_flag,      1},
            {"evaluate",    required_argument,  nullptr,        'e'},
            {"mode",        required_argument,  nullptr,        'm'},
            {"iterations",  required_argument,  nullptr,        'i'},
            {}
    };
    int i = 0, j = 0, c;
    std::string r{};
    while ((c = getopt_long(argc, argv, "e:m:i:", opts, &j)) != -1) {
        switch (c) {
            case '\0':
                break;
            case 'e':
                r = optarg;
                break;
            case 'm':
                cli_mode = static_cast<cli_mode_t>(strtol(optarg, nullptr, 0));
                break;
            case 'i':
                max_iterations = std::stoi(optarg);
                break;
            default:
                abort();
        }
    }
    if (r.empty() && cgi_flag) {
        std::string query{getenv("QUERY_STRING")};
        size_t start = 0, end = 0;
        for (size_t k = 1; k < query.size(); ++k) {
            if (query[k] == '=' && query[k - 1] == 'q' && (k == 1 || query[k - 2] == '&')) {
                start = k + 1;
                break;
            }
        }
        if (start == 0) {
            exit(1);
        }
        for (size_t k = start; k < query.size(); ++k) {
            if (query[k] == '&') {
                end = k;
                break;
            }
        }
        if (end == 0) {
            end = query.size();
        }
        std::ostringstream oss{};
        for (size_t k = start; k < end; ++k) {
            if (query[k] == '%' && end - k > 2) {
                oss << (char) std::stoi(query.substr(k + 1, 2), nullptr, 16);
                k += 2;
            } else {
                oss << query[k];
            }
        }
        r = oss.str();
    }
    return r;
}
