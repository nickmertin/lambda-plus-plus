#ifndef LAMBDA_CLI_H
#define LAMBDA_CLI_H


#include <string>

enum cli_mode_t {
    MODE_PURE,
    MODE_NUMERIC,
};

extern int verbose_flag;
extern int cgi_flag;
extern int max_iterations;
extern cli_mode_t cli_mode;

std::string read_cli(int argc, char **argv);


#endif //LAMBDA_CLI_H
