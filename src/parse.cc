#include "parse.h"

static constexpr uint64_t gen_charmap(int offset, const char src[]) {
    return src[0] ? (1ull << (src[0] - offset)) | gen_charmap(offset, src + 1) : 0ull;
};

static constexpr bool valid_var(char c) {
    constexpr uint64_t map[2] = {
            gen_charmap(0,  "$0123456789?"),
            gen_charmap(64, "@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz")
    };
    return static_cast<bool>(map[c >> 6] & (1ull << (c & 0x3F)));
}

static size_t read_variable(const std::string &input, size_t start) {
    for (size_t i = start; i < input.length(); ++i)
        if (!valid_var(input[i]))
            return i;
    return input.length();
}

static inline void append(std::shared_ptr<const lambda_term> &current, const std::shared_ptr<const lambda_term> &next) {
    current = current ? std::shared_ptr<const lambda_term>(new application_term(current, next)) : next;
}

const char *parse_exception::what() const noexcept {
    return "Invalid input format!";
}

std::shared_ptr<const lambda_term> parse(const std::string &input) {
    std::shared_ptr<const lambda_term> current{};
    for (size_t i = 0; i < input.length();) {
        switch (input[i]) {
            case '\\': {
                size_t var_end = read_variable(input, ++i);
                if (var_end == i || var_end == input.length() || input[var_end] != '.') {
                    throw parse_exception();
                }
                append(current, std::shared_ptr<const lambda_term>(new abstraction_term(input.substr(i, var_end - i), parse(input.substr(var_end + 1)))));
                return current;
            }
            case '(': {
                size_t j = i + 1;
                for (size_t k = 1; j < input.length() && k; ++j)
                    if (input[j] == '(')
                        ++k;
                    else if (input[j] == ')')
                        --k;
                append(current, parse(input.substr(i + 1, j - i - 2)));
                i = j;
                break;
            }
            case '[': {
                if (!current) {
                    throw parse_exception();
                }
                size_t j = i + 1;
                for (size_t k = 1; j < input.length() && k; ++j)
                    if (input[j] == '[')
                        ++k;
                    else if (input[j] == ']')
                        --k;
                size_t var_end = read_variable(input, ++i);
                if (var_end == i || var_end >= input.length() - 1 || input.substr(var_end, 2) != ":=") {
                    throw parse_exception();
                }
                current = std::shared_ptr<const lambda_term>(new substitution_term(current, input.substr(i, var_end - i), parse(input.substr(var_end + 2, j - var_end - 3))));
                i = j;
                break;
            }
            default:
                if (isspace(input[i])) {
                    ++i;
                } else {
                    size_t end = read_variable(input, i);
                    if (end == i) {
                        throw parse_exception();
                    }
                    append(current, std::shared_ptr<const lambda_term>(new variable_term(input.substr(i, end - i))));
                    i = end;
                }
                break;
        }
    }
    if (!current) {
        throw parse_exception();
    }
    return current;
}
