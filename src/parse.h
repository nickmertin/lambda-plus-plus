#ifndef LAMBDA_PARSE_H
#define LAMBDA_PARSE_H


#include <memory>
#include "lambda_term.h"

class parse_exception final : public std::exception {
public:
    const char *what() const noexcept override;
};

std::shared_ptr<const lambda_term> parse(const std::string &input);


#endif //LAMBDA_PARSE_H
